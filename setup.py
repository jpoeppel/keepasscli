#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of keepassCLI.


import setuptools
import os
import sys

if sys.argv[-1] == 'setup.py':
    print("To install, run 'python setup.py install'")
    print()

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setuptools.setup(
    name="keepassCLI",
    version=1.0,
    description="Command line interface for keepass (2) files.",
    long_description=read("README"),
    license='GNU Lesser General Public License v3 or later (LGPLv3+)',
    maintainer="Jan Pöppel",
    maintainer_email="jpoeppel@techfak.uni-bielefeld.de",
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    install_requires=[
          'libkeepass',
      ],
    dependency_links = ['https://github.com/libkeepass/libkeepass'],
    scripts = ["keepassCLI.py"],
    )
