#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Command line tool to query and read secrets from keepass 
vaults.

@author: jpoeppel
Last Modified: 11.05.17
"""

import libkeepass
import argparse
import getpass
import readline

import cmd
import subprocess

import io
import os
import sys

if hasattr(__builtins__, 'raw_input'):
    input=raw_input

"""
TODOS:
1. Unsalt only the required pw, not entire file
    posponed: Should be functionality by libkeepass, as we would have to 
    dublicate too much code here, see
    _get_entry_password for more information about the problem
2. Allow arbitrary unicode!
3. Consider a way to be able to search through the history as well!
    - Currently not desired
4. Consider sorting the fields if available, or having some default fields
    that are always displayed!
5. Allow path completion for selecting vault.
"""

HANDLE_CASE = """translate(.,"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "abcdefghijklmnopqrstuvwxyz")"""

class KeePassCMD(cmd.Cmd, object):

    def __init__(self, dbPath="", action="", quit=False, masterPW=""):
        super(KeePassCMD, self).__init__()
        self.dbPath = dbPath
        self.kdb = None
        self.masterPW = masterPW
        self._lastMatches = []
        self.prompt = "(keepassCLI) "
        if not dbPath and action == "help":
            self.do_help("")
        if action:
            parts = action.split(" ")
            funcName = "do_{}".format(parts[0])
            if funcName in dir(self):
                getattr(self, funcName)("".join(parts[1:]))
            if quit:
                print("keepassCLI will now quit.")
                sys.exit(0)

    def setup_db(self):
       if not os.path.isfile(self.dbPath):
           self.dbPath = input("Speficy a valid database file to load: ")
           if "~" in self.dbPath:
               self.dbPath = os.path.expanduser(self.dbPath)
       if not self.masterPW:
            self.masterPW = getpass.getpass(
                    prompt="Please enter your masterpassword: ")
       try:
            self.kdb = self._load_kdb()
       except IOError:
           print("Invalid vault file or master key.")
           sys.exit(-1)

    def _load_kdb(self):
        with io.open(self.dbPath, 'rb') as stream:
            signature = libkeepass.common.read_signature(stream)
            cls = libkeepass.get_kdb_reader(signature)
            return cls(stream, unprotect=False, password=self.masterPW)


    def postloop(self):
        if self.kdb:
            self.kdb.close()

    def _list_entries(self, root, showAll=False):
        """
            Prints all entries in the form:
                Title: <EntryTitle>       User: <Username>
            starting from the provided root element.
        """
        #Use Group/Entry in order to filter out History/Entry
        if showAll:
            entries = root.xpath("//Group/Entry")
        else:
            entries = root.xpath("./Entry")
        titles = []
        users = []
        maxLengthTitle = 0
        for e in entries:
            title = e.xpath("String/Key[text()='Title']" \
                      "/ancestor::String/Value")[0].text
            user = e.xpath("String/Key[text()='UserName']" \
                     "/ancestor::String/Value")[0].text
            maxLengthTitle = max(maxLengthTitle, len(title))
            titles.append(title)
            users.append(user)
        for t,u in zip(titles,users):
            t += " " * (maxLengthTitle - len(t))
            if u is None:
                u = ""
            print("Title: {}\t\tUser: {}".format(t.encode("utf-8"), u.encode("utf-8")))

    def do_listgrps(self, arg):
        """
            Lists all contained groups. Any provided arguments, will be ignored.
        """
        if not self.kdb:
            self.setup_db()
        groups = self.kdb.obj_root.xpath("//Group")
        if groups:
            print("Contained groups:")
        for g in groups:
            title = g.xpath("./Name[text()]")
            depth = len(g.xpath("./ancestor::Group"))
            if title:
                print(">>"*depth + title[0])

    def help_listgrps(self):
        print("Lists all groups in the vault.")


    def do_list(self, group):
        """
            List all entries within the specified group. If no group is given,
            all entries will be listed.
        """

        if not self.kdb:
            self.setup_db()
        if group:
            groups = self.kdb.obj_root.xpath("//Group/Name[text()" \
                        "[contains({},'{}')]]/ancestor::Group[position()=1]"
                        .format(HANDLE_CASE, group.lower()))
            if len(groups) > 0:
                #Should only ever be one
                for g in groups:
                    print("Entries for group {}:".format(
                            g.xpath("./Name[text()]")[0]))
                    self._list_entries(g)

                    containedGroups = g.xpath("./Group")
                    if containedGroups:
                        print("-------------------------------------------------")
                        print("This group furthermore contains these subgroups: ")
                        for subG in containedGroups:
                            print(subG.xpath("./Name[text()]")[0])
            else:
                print("There is no group named '{}'".format(group))
        else:
            print("Print list")
            self._list_entries(self.kdb.obj_root, showAll=True)

    def help_list(self):
        print("Usage: list [group]")
        print("List all entries within the specified group. " \
              "If 'group' is not specified, all entries will be listed")

    def _get_entry_password(self, entry):
        """
            TODO Ideally only unprotect the pwd-key, however this requires to 
            figure out the correct counter and buffer for the salsa encryption!
            I would need to collect the lengths of all protected salts of the 
            entries before this one, compute their salt_buffers, concatenate 
            them with the excess buffer of the previous buffer before I reach 
            the actual entry. It might be easier to patch libkeepass, to simply 
            not compute the final xor or store the unprotected password back 
            into the tree.
        """
        self.kdb.unprotect()
        pwd = entry.xpath("String/Key[text()='Password']" \
                                      "/ancestor::String/Value")[0].text
        self.kdb.protect()
        return pwd

    def _get_entry_username(self, entry):
        user = entry.xpath("String/Key[text()='UserName']" \
                                       "/ancestor::String/Value")[0].text
        return user

    def _get_entry_title(self, entry):
        title = entry.xpath("String/Key[text()='Title']" \
                                        "/ancestor::String/Value")[0].text
        return title

    def _read_entry(self, targetEntry):
        res = {}
        fields = targetEntry.xpath("String")
        for f in fields:
            if f.Value:
                res[f.Key] = f.Value.text
        return res

    def _read_group(self, targetEntry):
        return targetEntry.xpath("./ancestor::Group[position()=1]/Name")[0].text

    def _select_entry(self, targetEntry):
        """
            Tries to find an entry that matches the specified targetEntry. 
            Will perform a "contains()" search on all entry titles, 
            ignoring cases.
            This will also ignore searching the histories of entries!
        """
        if not self.kdb:
            self.setup_db()
        entries = self.kdb.obj_root.xpath("//Group/Entry/String/Key"\
                            "[text()='Title']/ancestor::String/Value[text()"\
                            "[contains({},'{}')]]/ancestor::Entry"
                            .format(HANDLE_CASE, targetEntry.lower()))

        try:
            #If a number was passed, try adding the index of the last matches
            entryNumber = int(targetEntry)
            entries.append(self._lastMatches[entryNumber])
        except (ValueError, IndexError):
            pass
        if len(entries) == 1:
            userdict = self._read_entry(entries[0])
#            user = self._get_entry_username(entries[0])
#            pwd = self._get_entry_password(entries[0])
#            title = self._get_entry_title(entries[0])
        elif len(entries) > 1:
            print("Multiple matches found for entry: '{}':".format(targetEntry))
            for i,e in enumerate(entries):
                userdict = self._read_entry(e)
#                user = self._get_entry_username(e)
                user = userdict.get("UserName", "None")
#                title = self._get_entry_title(e)
                title = userdict.get("Title", "None")
                group = self._read_group(e)
                filling1 = " "*(20-len(title)-i//10)
                filling2 = " "*(20-len(user)-i//10)
                print("{}: Title: {}{}User: {}{}Group: {}".format(i,title,filling1, user, filling2, group))
                if i == len(entries) - 2:
                    print("-"*30)
                    print("Entry from the last search:")
            try:
                numberString = input("Select the desired entry: ")
            except EOFError:
                print("<Aborted>")
                userdict = None
#                user = pwd = title = None
            else:    
                try:
                    number = int(numberString)
                except ValueError:
                    print("{} is not a valid number".format(numberString))
                    userdict = None
                else:
                    try:
                        userdict = self._read_entry(entries[number])
#                        user = self._get_entry_username(entries[number])
#                        pwd = self._get_entry_password(entries[number])
#                        title = self._get_entry_title(entries[number])
                    except IndexError:
                        print("You selected an invalid number.")
                        userdict= None
#                        user = pwd = title = None
        else:
            print("No entry named '{}' found".format(targetEntry))
            userdict= None
#            user = pwd = title= None
#        return user, pwd, title
        return userdict

    def do_print(self, entry):
        """
            Prints the specified secret entry.
        """
        userdict = self._select_entry(entry)
        if userdict is None:
            return
        print("Credentials for Entry '{}'".format(userdict.get("Title", "None")))
        for key, val in userdict.items():
            if key != "Title":
                print("{}: {}".format(key, val))
#        print("User: {}\nPassword: {}".format(user, pwd))

    def do_test(self, a,b="b"):
        print("a: {}, b: {}".format(a,b))

    def help_print(self):
        print("Usage: print <entry>")
        print("Will print 'User: <Username> Password: <Password>' for all " \
              "entries matching 'entry'.")

    def do_toclipboard(self, entry):
        """
            Copies the specified secret to the clipboard.
        """
        if entry:
            # user, pwd, title = self._select_entry(entry)
            userdict = self._select_entry(entry)
            print("Userdict: ", userdict)
            user = userdict.get("UserName", "None")
            pwd = userdict.get("Password", None)
            title = userdict.get("Title", "None")
            if pwd is None:
                return
            if sys.platform.startswith('linux'):
                p = subprocess.Popen(['xclip','-selection','c'], 
                                     stdin=subprocess.PIPE,
                                     close_fds=True)
            elif sys.platform == 'darwin':
                p = subprocess.Popen('pbcopy',
                                     env={'LANG': 'en_US.UTF-8'},
                                     stdin=subprocess.PIPE)
            else:
                print("This platform is currently not supported.")
                return
            p.communicate(input="{}".format(pwd).encode('utf-8'))
            print("Password for entry: '{}' has been copied to your clipboard."\
                  " The Username is: {}".format(title, user))

    def help_toclipboard(self):
        print("Usage: toclipboard <entry>")
        print("Copies the specified password to the clipboard. Currently Linux " \
              "(through xclip) and MacOs (through pbcopy) are supported.")

    def do_search(self, keyword):
        """
            Searches for entries matching the given keyword.
        """
        if not self.kdb:
            self.setup_db()
        matches = self.kdb.obj_root.xpath("//String/Value[text()" \
                                        "[contains({},'{}')]]"
                                        .format(HANDLE_CASE,keyword))
        lastMatches = []
        i = 0
        for m in matches:
            if len(m.xpath("./ancestor::History")) > 0:
                continue
            matchText = m.text
            matchField = m.xpath("./ancestor::String[position()=1]/Key")[0].text
            matchEntry = m.xpath("./ancestor::Entry[position()=1]")[0]
            
            lastMatches.append(matchEntry)
            title = self._get_entry_title(matchEntry)
            print("{}: Entry: '{}', match in field '{}': {}"
                  .format(i, title, matchField, matchText))
            i+=1
        self._lastMatches = lastMatches 
        if len(matches) == 0:
            print("Could not find any entry that contained {} "\
                  "in any of it's tags.".format(keyword))

    def help_search(self):
        print("Usage: search <keyword>")
        print("Searches all values (e.g. Title, Username, URL...) " \
              "within the vault for the specified keyword and lists " \
              "all matching entries.")

    def do_exit(self, s):
        print("keepassCLI will now quit.")
        return True

    def help_exit(self):
        print("Exit the interpreter.")
        print("Your can also use the Ctrl-D shortcut.")

    do_EOF = do_exit
    help_EOF = help_exit




if __name__ == "__main__":
    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    parser = argparse.ArgumentParser(description="Interactive CLI tool to " \
                                     "query keepass2 vaults for stored " \
                                     "credentials.")
    parser.add_argument("-v", "--vault", default="", type=str, 
                        help="Path to the vault to open.")
    parser.add_argument("-a", "--action", default="", type=str, 
                        help="Action to perform on the vault. All available actions can be " \
                        "queried using '-a help'.")
    parser.add_argument("-q", "--quit", default=False, action="store_true", 
                        help="This will quit the tool after an action has been "\
                        "performed. Will be ignored, if no action was " \
                        "specified.")
    args = parser.parse_args()

    try:
        KeePassCMD(dbPath=args.vault, action=args.action, quit=args.quit).cmdloop()
    except KeyboardInterrupt:
        print("^C")
        print("keepassCLI will now quit.")
        
